<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Shop</title>
    <#include "./css/reset.css">
    <#include "./css/style-cart.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


</head>

<body>
<header class="header">
    <div class="container">
        <div class="header-wrapper">
            <h1 class="main-title">${name}</h1>
            <nav class="navbar">
                <a href="/shop" class="nav-link">Go to catalog</a>
                <a href="/logout" class="nav-link">Log out</a>
            </nav>
        </div>
    </div>
</header>
<section class="catalog">
    <div class="container">

        <h2 class="catalog-title">my cart</h2>

        <ul class="catalog-list">
            <li class="catalog-list_header catalog-list_item">
                <span class="item item-id">Product ID</span>
                <span class="item item-categoty">Category</span>
                <span class="item item-name">Product name</span>
                <span class="item item-price">Price</span>
                <span class="item item-quantity">Quantity</span>
                <div class="item item-actions"></div>
            </li>
    <#list cart as listItem>
        <li class="catalog-list_item">
            <span class="item item-id">${listItem.item_id}</span>
            <span class="item item-categoty">${listItem.category}</span>
            <span class="item item-name">${listItem.name}</span>
            <span class="item item-price">${listItem.price}</span>
            <span class="item item-quantity">${listItem.quantity}</span>

            <div class="item item-actions">
                <form action="/cart" method="post">
                    <input type="hidden" value=${listItem.id} name="id">
                    <input type="hidden" value="inc" name="action">
                    <button class = "item-actions-btn" type="submit">+</button>
                </form>
                <form action="/cart" method="post">
                    <input type="hidden" value=${listItem.id} name="id">
                    <input type="hidden" value="dec" name="action">
                    <button class = "item-actions-btn" type="submit">-</button>
                </form>
                <form action="/cart" method="post">
                    <input type="hidden" value=${listItem.id} name="id">
                    <input type="hidden" value="remove" name="action">
                    <button class = "item-actions-btn" type="submit">x</button>
                </form>
            </div>

        </li>

    </#list>
            <li class="catalog-list_footer catalog-list_item">
                <span class="item item-id"></span>
                <span class="item item-categoty"></span>
                <span class="item item-name">Total:</span>
                <span class="item item-price">${summ}</span>
                <span class="item item-quantity">${qntSum}</span>
                <div class="item item-actions"></div>
            </li>
        </ul>


    </div>
</section>

</body>
</html>