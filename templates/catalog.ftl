<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Shop</title>
    <#include "css/reset.css">
    <#include "css/style.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


</head>

<body>
<header class="header">
    <div class="container">
        <div class="header-wrapper">
            <h1 class="main-title">My shop</h1>
            <nav class="navbar">
                <#if registered??>

                    <a href="/logout" class="nav-link"><i class="fas fa-sign-out-alt"></i> Log out </a>
                    <a href="/cart" class="nav-link"><i class="fas fa-shopping-cart"></i> Go to cart</a>
                <#else>
                    <a href="/login" class="nav-link">Log in</a>
                    <a href="/registration" class="nav-link">Registration</a>
                </#if>

            </nav>
        </div>

    </div>
</header>
<section class="catalog">
    <div class="container">

        <h2 class="catalog-title">Catalog</h2>

        <ul class="catalog-list">
            <li class="catalog-list_header catalog-list_item">
                <span class="item item-id">Product ID</span>
                <span class="item item-categoty">Category</span>
                <span class="item item-name">Product name</span>
                <span class="item item-price">Product price</span>
                <div class="item"></div>
            </li>
    <#list products as listItem>
        <li class="catalog-list_item">
            <span class="item item-id">${listItem.id}</span>
            <span class="item item-categoty">${listItem.category}</span>
            <span class="item item-name">${listItem.name}</span>
            <span class="item item-price">${listItem.price}</span>
        <#if registered??>
            <div class="item">
                <form action="/shop" method="post">
                    <input type="hidden" value=${listItem.id} name="item_id">
                    <button type="submit"><i class="fas fa-cart-plus"></i> add to cart</button>
                </form>
            </div>
        <#else>
        <div class="item"></div>
        </#if>
        </li>

    </#list>
        </ul>


    </div>
</section>

</body>
</html>