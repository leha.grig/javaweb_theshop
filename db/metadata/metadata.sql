create table if not exists alex_grig_goods
(
	itemId int auto_increment,
	itemCategory varchar(32) null,
	itemName varchar(32) null,
	itemPrice int null,
	constraint alex_grig_goods_itemId_uindex
		unique (itemId)
)
;

alter table alex_grig_goods
	add primary key (itemId)
;

create table if not exists alex_grig_users
(
	userId int default '0' not null,
	userName varchar(32) null,
	userSurname varchar(32) null,
	userPass varchar(32) not null,
	userLogin varchar(32) not null,
	constraint alex_grig_users_userId_uindex
		unique (userId),
	constraint alex_grig_users_userLogin_uindex
		unique (userLogin)
)
;

alter table alex_grig_users
	add primary key (userId)
;

create table if not exists alex_grig_carts
(
	id int auto_increment,
	user_id int null,
	item_id int null,
	quantity int null,
	constraint alex_grig_carts_id_uindex
		unique (id),
	constraint alex_grig_carts_alex_grig_goods_itemId_fk
		foreign key (item_id) references alex_grig_goods (itemId)
			on delete cascade,
	constraint alex_grig_carts_alex_grig_users_userId_fk
		foreign key (user_id) references alex_grig_users (userId)
			on delete cascade
)
;

alter table alex_grig_carts
	add primary key (id)
;