package services;


import DAO.CartsDAO_SQL;
import objects.CartItem;
import objects.CartItemExtra;

import java.util.List;

public class CartService {
    private final CartsDAO_SQL carts;

    public CartService(CartsDAO_SQL carts) {
        this.carts = carts;
    }

    public void add (CartItem elem){
        carts.add(elem);
    }

    public void addChecked(int user_id, int item_id) {

        final int[] id = {0};

        carts.getByUser(user_id).stream()
                .filter(ci -> ci.getItem_id() == item_id)
                .findFirst()
                .ifPresent(ci -> id[0] = ci.getId());

        if (id[0] == 0){
            carts.add(new CartItem(user_id, item_id, 1));
        }
        else {
            carts.incQuantity(id[0]);
        }
    }

    public List<CartItem> getAll() {
        return carts.getAll();
    }

    public CartItem get(int id) {
        return carts.get(id);
    }

    // TODO перехват ElementNotFoundInDbException
    public void remove(int id) {
        carts.remove(id);
    }

    public boolean isEmpty() {
        return carts.isEmpty();
    }

    // TODO перехват IllegalArgumentException
    public void setQuantity(int quantity, int id){
        carts.setQuantity(quantity, id);
    }

    public void incQuantity(int id){
        carts.incQuantity(id);
    }

    // TODO перехват ElementNotFoundInDbException
    public void decQuantity(int id) {
        carts.decQuantity(id);
    }

    public List<CartItemExtra> getByUser(int user_id){
        return carts.getByUser(user_id);
    }
}
