package services;

import DAO.DAO;
import objects.Goods;

import java.util.List;

public class GoodsService {
    private DAO<Goods> goods;

    public GoodsService(DAO goods) {
        this.goods = goods;
    }

    public void addItem(Goods item) {
        goods.add(item);
    }

    public void addItem (String category, String name, int price) {
        Goods item = new Goods(category, name, price);
        goods.add(item);
    }

    public void removeItem (int id){ // TODO перехват ElementNotFoundInDbException
        goods.remove(id);
    }

    public List<Goods> getAll() {
        return goods.getAll();
    }

    public Goods getItem (int itemId){ // TODO перехват ElementNotFoundInDbException
        return goods.get(itemId);
    }

    public boolean isGoodsDbEmpty (){
        return goods.isEmpty();
    }
}
