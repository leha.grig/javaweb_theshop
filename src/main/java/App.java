import DAO.CartsDAO_SQL;
import DAO.DAO;
import DAO.GoodsDAO_SQL;
import DAO.UserDAO_SQL;
import database.DbConnection;
import filters.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import services.CartService;
import services.GoodsService;
import services.UserService;
import servlets.*;
import utils.CookieProcessor;

import javax.servlet.DispatcherType;
import java.sql.Connection;
import java.util.EnumSet;


// TODO смотерть https://github.com/alexr007/dantests по классам-фильтрам для сервлета

// TODO логгер - общий класс сервлетРуут с протектед логгером, остальные сервлеты его экстендят

// TODO основной конструктор - ближе к коду


public class App {


    public static void main(String[] args) throws Exception {
        String cookieName = CookiesNames.SHOP.getName();
        Connection conn = new DbConnection().connection();


        DAO users = new UserDAO_SQL(conn);
        GoodsDAO_SQL goods = new GoodsDAO_SQL(conn);
        CartsDAO_SQL carts = new CartsDAO_SQL(conn);
        UserService userService = new UserService(users);
        GoodsService goodsService = new GoodsService(goods);
        CartService cartService = new CartService(carts);
        CookieProcessor cp = new CookieProcessor(cookieName, userService);
        CartServlet cartServlet = new CartServlet(cartService, userService, cp);
        ServletShop servletShop = new ServletShop(goodsService, cartService, cp);
        ServletRegistration servletRegistration = new ServletRegistration(userService);
        ServletLogin servletLogin = new ServletLogin();
        ServletLogout servletLogout = new ServletLogout();
        AssetsServlet assetsServlet = new AssetsServlet();
        ConsoleApp consoleApp = new ConsoleApp(cartService, userService, goodsService);


        System.out.println(userService.getAll());

//        consoleApp.run();

        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(servletShop), "/shop");
        handler.addServlet(new ServletHolder(servletRegistration), "/registration");
        handler.addServlet(new ServletHolder(servletLogin), "/login");
        handler.addServlet(new ServletHolder(servletLogout), "/logout");
        handler.addServlet(new ServletHolder(cartServlet), "/cart");
        handler.addServlet(new ServletHolder(assetsServlet), "/assets/*");

        handler.addFilter(new FilterHolder(new NoUsersFilter(userService)), "/login", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new ExistingLoginFilter(userService)), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new EntryFormatFilter()), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new PasswordMismatchFilter()), "/registration", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginPasswordFilter(userService)), "/login", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new HasCookiesFilter()), "/cart", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new HasMyCookieFilter()), "/cart", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new CookieMatchFilter(cp)), "/cart", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));



        Server server = new Server(80);
        server.setHandler(handler);
        server.start();
        server.join();
    }
}
