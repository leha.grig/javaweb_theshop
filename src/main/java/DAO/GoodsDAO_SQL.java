package DAO;

import exceptions.ElementNotFoundInDbException;

import objects.Goods;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GoodsDAO_SQL implements DAO<Goods> {
    private final Connection conn;

    public GoodsDAO_SQL(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void add(Goods item) {
        try {
            PreparedStatement stmt = conn.prepareStatement("insert into alex_grig_goods(itemCategory, itemName, itemPrice) values (?, ?, ?)");
            stmt.setString(1, item.getCategory());
            stmt.setString(2, item.getName());
            stmt.setInt(3, item.getPrice());
            stmt.execute();
        } catch (SQLException e) {
            throw new IllegalArgumentException("something went wrong", e);
        }
    }

    @Override
    public List<Goods> getAll() {
        List<Goods> goods = new ArrayList<>();
        try {
            String sql = "SELECT * FROM alex_grig_goods";
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rSet = stm.executeQuery();
            while (rSet.next()) {
                Goods item = new Goods(
                        rSet.getInt("itemId"),
                        rSet.getString("itemCategory"),
                        rSet.getString("itemName"),
                        rSet.getInt("itemPrice")
                );
                goods.add(item);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Something went wrong");
        }
        return goods;
    }

    @Override // TODO перехват ElementNotFoundInDbException
    public Goods get(int id) {
        try {
            PreparedStatement stmt = conn.prepareStatement("select * from alex_grig_goods where itemId = ?");
            stmt.setInt(1, id);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Goods(
                        resultSet.getInt("itemId"),
                        resultSet.getString("itemCategory"),
                        resultSet.getString("itemName"),
                        resultSet.getInt("itemPrice")
                );
            } else {
                throw new ElementNotFoundInDbException();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
    }

    @Override // TODO перехват ElementNotFoundInDbException
    public void remove(int id) {
        try {
            get(id);
            PreparedStatement stmt = conn.prepareStatement("delete from alex_grig_goods where itemId = ?");
            stmt.setInt(1, id);
            stmt.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
    }

    @Override
    public boolean isEmpty() {
        int count = 0;
        try {
            Statement stmt = conn.createStatement();
            String query = "select count(*) from alex_grig_goods";
            ResultSet resultSet = stmt.executeQuery(query);
            while(resultSet.next()){
                count = resultSet.getInt("COUNT(*)");
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
        return count == 0;
    }
}
