package DAO;

import exceptions.LoginMatchException;
import exceptions.ElementNotFoundInDbException;
import objects.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO_SQL implements DAO<User> {
    private final Connection conn;

    public UserDAO_SQL(Connection conn) {
        this.conn = conn;
    }


    @Override
    public void add(User user) { // TODO перехват LoginMatchException
        try {
            get(user.getId());
            throw new LoginMatchException("The user with this login is already exist");
        } catch (ElementNotFoundInDbException ex) {
            try {
                PreparedStatement stmt = conn.prepareStatement("insert into alex_grig_users(userId, userName, userSurname, userPass, userLogin) values (?, ?, ?, ?, ?)");
                stmt.setInt(1, user.getId());
                stmt.setString(2, user.getName());
                stmt.setString(3, user.getSurname());
                stmt.setString(4, user.getPassword());
                stmt.setString(5, user.getLogin());
                stmt.execute();
            } catch (SQLException e) {
                throw new IllegalArgumentException("something went wrong", e);
            }
        }
    }

    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try {
            String sql = "SELECT * FROM alex_grig_users";
            PreparedStatement stm = conn.prepareStatement(sql);
            ResultSet rSet = stm.executeQuery();
            while (rSet.next()) {
                User user = new User(
                        rSet.getString("userName"),
                        rSet.getString("userSurname"),
                        rSet.getString("userLogin"),
                        rSet.getString("userPass")
                );
                users.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Something went wrong");
        }
        return users;
    }

    @Override
    public User get(int userId) { // TODO перехват ElementNotFoundInDbException
        try {
            PreparedStatement stmt = conn.prepareStatement("select * from alex_grig_users where userId = ?");
            stmt.setInt(1, userId);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new User(
                        resultSet.getString("userName"),
                        resultSet.getString("userSurname"),
                        resultSet.getString("userLogin"),
                        resultSet.getString("userPass")
                );
            } else {
                throw new ElementNotFoundInDbException();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
    }

    @Override
    public void remove(int id) { // TODO перехват ElementNotFoundInDbException
        try {
            get(id);
            PreparedStatement stmt = conn.prepareStatement("DELETE from alex_grig_users where userId = ?");
            stmt.setInt(1, id);
            stmt.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
    }

    @Override
    public boolean isEmpty() {
        int count = 0;
        try {
            Statement stmt = conn.createStatement();
            String query = "select count(*) from alex_grig_users";
            ResultSet resultSet = stmt.executeQuery(query);
            while(resultSet.next()){
               count = resultSet.getInt("COUNT(*)");
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException("smth went wrong", e);
        }
        return count == 0;
    }
}
