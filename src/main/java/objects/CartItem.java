package objects;

public class CartItem implements Identifiable {
    private int id;
    private int user_id;
    private int item_id;
    private int quantity;

    public CartItem(int user_id, int item_id, int quantity) {
        this(-1, user_id, item_id, quantity);
    }

    public CartItem(int id, int user_id, int item_id, int quantity) {
        this.id = id;
        this.user_id = user_id;
        this.item_id = item_id;
        this.quantity = quantity;
    }
    @Override
    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return  '{'+"id=" + id +
                ", user_id=" + user_id +
                ", item_id=" + item_id +
                ", quantity=" + quantity +
                '}';
    }
}
