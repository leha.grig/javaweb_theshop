package objects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersCarts {

    private Map<Integer, List<Goods>> usersCarts = new HashMap<>();

    public List<Goods> getUserCart (int userId){
        return usersCarts.get(userId);
    }

    public void addUser (int userId, List<Goods> list) {
        usersCarts.put(userId, list);
    }

    public void addItemInUserCart(int userId, Goods good){
        List<Goods> goods = usersCarts.get(userId);
        goods.add(good);
        usersCarts.put(userId, goods);
    }

}
