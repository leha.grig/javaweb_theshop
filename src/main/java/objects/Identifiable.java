package objects;

public interface Identifiable {
    int getId();
}
