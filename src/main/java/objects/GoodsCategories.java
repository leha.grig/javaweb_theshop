package objects;

public enum GoodsCategories {
    NOTEBOOK(), SMARTPHONE(), MONITOR(), DIGITAL_CAMERA(), TV()
}
