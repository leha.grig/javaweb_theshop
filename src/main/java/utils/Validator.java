package utils;

import exceptions.IncorrectEntryException;

public interface Validator <T> {
    boolean isValid (T elem) throws IncorrectEntryException;
}
