package servlets;

import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ServletLogin extends HttpServlet {

    private final String cookieName = CookiesNames.SHOP.getName();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Files.copy(Paths.get("./templates/formLogin.html"), resp.getOutputStream());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);

        String login = pfr.getString("login");

        resp.addCookie(new Cookie(cookieName, String.valueOf(login.hashCode())));
        resp.sendRedirect("/shop");
    }
}
