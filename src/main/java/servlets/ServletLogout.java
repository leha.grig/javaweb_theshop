package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletLogout extends HttpServlet {
    private final String cookieName = CookiesNames.SHOP.getName();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = new Cookie(cookieName, "");
        cookie.setMaxAge(0);
       resp.addCookie(cookie);
       resp.sendRedirect("/shop");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        Cookie cookie = new Cookie(cookieName, "");
//        cookie.setMaxAge(0);
//        resp.addCookie(cookie);
//        resp.sendRedirect("/login");
    }
}
