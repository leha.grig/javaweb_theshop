package servlets;


import com.sun.deploy.net.cookie.CookieUnavailableException;
import services.CartService;
import services.GoodsService;
import objects.*;
import utils.CookieProcessor;
import utils.Freemarker;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class ServletShop extends HttpServlet {

    private final GoodsService goodsService;
    private final CartService cartService;
    private final CookieProcessor cp;

    public ServletShop(GoodsService goodsService, CartService cartService, CookieProcessor cp) {
        this.goodsService = goodsService;
        this.cartService = cartService;
        this.cp = cp;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Freemarker f = new Freemarker();

        List<Goods> a2 = goodsService.getAll();
        HashMap<String, Object> data = new HashMap<>();
        data.put("products", a2);
        if (cp.cookieMatch(req)) {
            data.put("registered", "true");
        }
        f.render("catalog.ftl", data, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);
        int item_id = pfr.getInt("item_id");

        try {
            int user_id = Integer.parseInt(cp.getValue(req));
            cartService.addChecked(user_id, item_id);

        } catch (CookieUnavailableException | NumberFormatException e) {
            resp.getWriter().printf("<html> <a href=\"/shop\"> You have made a request in illegal way! %n %s </a></html>", e.getMessage());
        }
        doGet(req, resp);
    }

}

