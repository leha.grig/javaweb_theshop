package servlets;


import com.sun.deploy.net.cookie.CookieUnavailableException;
import services.CartService;
import services.UserService;
import objects.*;
import utils.CookieProcessor;
import utils.Freemarker;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class CartServlet extends HttpServlet {
    private final CartService cartService;
    private final UserService userService;
    private final CookieProcessor cp;

    public CartServlet(CartService cartService, UserService userService, CookieProcessor cp) {
        this.cartService = cartService;
        this.userService = userService;
        this.cp = cp;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Freemarker f = new Freemarker();
        int user_id = -1;
        try {
            user_id = Integer.parseInt(cp.getValue(req));
        } catch (CookieUnavailableException | NumberFormatException e) {
            resp.getWriter().printf("<html> <a href=\"/shop\"> You have tried to enter to cart in illegal way! %n %s </a></html>", e.getMessage());
        }
        String userName = userService.getUser(user_id).getLogin();

        List<CartItemExtra> cart = cartService.getByUser(user_id);
        int summ = cart.stream().mapToInt(cie -> cie.getPrice() * cie.getQuantity()).sum();
        int qntSum = cart.stream().mapToInt(CartItem::getQuantity).sum();
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", userName);
        data.put("cart", cart);
        data.put("qntSum", qntSum);
        data.put("summ", summ);

        f.render("cart.ftl", data, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);
        int id = pfr.getInt("id");
        String action = pfr.getString("action");

        switch (action) {
            case "inc":
                cartService.incQuantity(id);
                break;
            case "dec":
                cartService.decQuantity(id);
                break;
            case "remove":
                cartService.remove(id);
                break;
        }

        resp.sendRedirect("/cart");
    }
}
