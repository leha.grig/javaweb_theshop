package servlets;

import objects.User;
import services.UserService;
import utils.ParameterFromRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ServletRegistration extends HttpServlet {
    private UserService userService;


    public ServletRegistration(UserService userService) {
        this.userService = userService;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Files.copy(Paths.get("./templates/formAuthorization.html"), resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ParameterFromRequest pfr = new ParameterFromRequest(req);
        String name = pfr.getString("name").trim();
        String surname = pfr.getString("surname").trim();
        String login = pfr.getString("login").trim();
        String password = pfr.getString("password").trim();

        User user = new User(name, surname, login, password);
        userService.addUser(user);


        resp.sendRedirect("/login");
    }
}
