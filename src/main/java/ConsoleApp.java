import exceptions.ElementNotFoundInDbException;
import exceptions.IncorrectEntryException;
import exceptions.LoginMatchException;
import services.CartService;
import services.GoodsService;
import services.UserService;
import utils.EntryFormats;
import utils.StringValidator;
import utils.Validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ConsoleApp {
    private final Scanner sc = new Scanner(System.in);
    private final CartService cartService;
    private final UserService userService;
    private final GoodsService goodsService;
    private int user_id = -1;
    private List<String> iniMenu = new ArrayList<>(Arrays.asList("Login", "Registration", "Exit"));
    private List<String> mainMenu = new ArrayList<>(Arrays.asList("1. Catalog", "2. Add item to cart", "3. Show your cart", "4. Log out"));
    private Validator loginValidator = new StringValidator(EntryFormats.LOGIN.getFormat(), EntryFormats.LOGIN.getMessage());
    private Validator passValidator = new StringValidator(EntryFormats.PASSWORD.getFormat(), EntryFormats.PASSWORD.getMessage());
    private Validator nameValidator = new StringValidator(EntryFormats.NAME.getFormat(), EntryFormats.NAME.getMessage());


    public ConsoleApp(CartService cartService, UserService userService, GoodsService goodsService) {
        this.cartService = cartService;
        this.userService = userService;
        this.goodsService = goodsService;

    }

    public void run() {
        boolean flag = true;
        while (flag) {
            System.out.println("Type your choice:");
            System.out.println();
            iniMenu.forEach(System.out::println);
            String result = sc.nextLine().toLowerCase().trim();
            switch (result) {
                case "login":
                    loginCase();
                    break;
                case "registration":
                    registrationCase();
                    break;
                case "exit":
                    flag = false;
                    break;
                default:
                    System.out.println("Please, type a command from a list!");
            }
        }
    }

    private void registrationCase() {
        System.out.println("Enter your name");
        String name = sc.nextLine();
        System.out.println("Enter your surname");
        String surname = sc.nextLine();
        System.out.println("Enter your login");
        String login = sc.nextLine();
        System.out.println("Enter your password (from 6 to 25 symbols, spaces are not allowed)");
        String pass = sc.nextLine();
        try {
            nameValidator.isValid(name);
            nameValidator.isValid(surname);
            loginValidator.isValid(login);
            passValidator.isValid(pass);
        } catch (IncorrectEntryException e) {
            System.out.println(e.getMessage());
            return;
        }
        try {
            userService.addUser(name, surname, login, pass);
        } catch (LoginMatchException e){
            System.out.println(e.getMessage());
        }
    }

    private void loginCase() {
        System.out.println("Enter your login");
        String login = sc.nextLine();
        System.out.println("Enter your password");
        String pass = sc.nextLine();
        try {
            if (userService.checkUserPass(login, pass)) {
                user_id = login.hashCode();
                showMain();
            } else {
                throw new ElementNotFoundInDbException();
            }
        } catch (ElementNotFoundInDbException e) {
            System.out.println("Login or password is incorrect");
        }
    }

    private void showMain() {
        boolean flag2 = true;
        while (flag2) {
            System.out.println("Enter the number of the command (1-4):");
            mainMenu.forEach(System.out::println);
            String result = sc.nextLine().trim();
            switch (result) {
                case "1":
                    showCatalog();
                    break;
                case "2":
                    addToCart();
                    break;
                case "3":
                    showCart();
                    break;
                case "4":
                    user_id = -1;
                    flag2 = false;
                    break;
                default:
                    System.out.println("Please, type a command number from 1 to 4!");
            }
        }
    }

    private void showCart() {
        System.out.println("Your cart");
        System.out.printf("%-8s%-25s%-25s%-12s%-12s%n", "id", "Category", "Name", "Price", "Quantity");
        cartService.getByUser(user_id).forEach(System.out::println);
    }

    private void addToCart() {
        System.out.println("Enter the product id from the catalog you want to add to the cart:");
        try {
            int item_id = Integer.parseInt(sc.nextLine());
            cartService.addChecked(user_id, item_id);
        } catch (IllegalArgumentException e) {
            System.out.println("the product id must be a number from id column in the product catalog");
        }
    }

    private void showCatalog() {
        System.out.println("Catalog");
        System.out.printf("%-8s%-25s%-25s%-12s%n", "id", "category", "name", "price");
        goodsService.getAll().forEach(g -> System.out.println(g.fineToString()));
    }
}
