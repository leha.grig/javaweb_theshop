package exceptions;

public class IncorrectEntryException extends Exception {
    public IncorrectEntryException (String s){super(s);}
}
